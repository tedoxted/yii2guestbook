<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guestbook".
 *
 * @property integer $id
 * @property integer $auth_id
 * @property string $title
 * @property string $cont
 * @property string $created
 * @property string $updated
 */
class Guestbook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guestbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_id', 'cont', 'created', 'updated'], 'required'],
            [['auth_id'], 'integer'],
            [['cont'], 'string'],
            [['created', 'updated'], 'safe'],
            [['title'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_id' => 'Auth ID',
            'title' => 'Title',
            'cont' => 'Cont',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }
}
