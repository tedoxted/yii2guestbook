<?php
/**
 * Created by PhpStorm.
 * User: TED
 * Date: 12.12.2016
 * Time: 18:35
 */

namespace app\controllers;
use Yii;
use yii\web\Controller;
use app\models\Guestbook;


class GuestbookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionIndex(){
        $model = new Guestbook();
        return $this->render('view', [
            'model' => $model,
        ]);
    }
}